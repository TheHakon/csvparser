package download;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;

public class FileLoader {

	public void downloadFile() throws MalformedURLException, IOException {
		
			FileUtils.copyURLToFile(new URL(
					"https://docs.google.com/spreadsheets/d/1yzExHDrjWwm26CoQrNAYBCYfueBUtqNBkGmxuwMzsyk/export?format=csv&id=1yzExHDrjWwm26CoQrNAYBCYfueBUtqNBkGmxuwMzsyk&gid=1988975944"),
					new File(System.getProperty("user.dir").concat("\\data\\data.csv")));
	}

}

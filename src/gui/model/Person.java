package gui.model;

public class Person {

	public Person(String aktivitšt, String spielername, String discordname, String plattformpc, String plattformps4,
			String plattformxbox) {

		this.aktivitšt = aktivitšt;
		this.spielername = spielername;
		this.discordname = discordname;
		this.plattformpc = plattformpc;
		this.plattformps4 = plattformps4;
		this.plattformxbox = plattformxbox;
	}

	private String aktivitšt;
	private String spielername;
	private String discordname;
	private String plattformpc;
	private String plattformps4;
	private String plattformxbox;

	public String getAktivitšt() {
		return aktivitšt;
	}

	public String getSpielername() {
		return spielername;
	}

	public String getDiscordname() {
		return discordname;
	}

	public String getPlattformpc() {
		return plattformpc;
	}

	public String getPlattformps4() {
		return plattformps4;
	}
	
	public String getPlattformxbox() {
		return plattformxbox;
	}

}

package gui;

import gui.controller.LogInController;
import gui.controller.MainStageController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MainStage extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	private Pane mainPane = new Pane();
	private VBox vbox = new VBox();
	private MenuBar menuBar = new MenuBar();
	private Menu datei = new Menu("Datei");
	private MenuItem laden = new MenuItem("Laden");
	private MenuItem speichern = new MenuItem("Speichern");
	private MenuItem dashboard = new MenuItem("Dashboard");
	private BarChart<String, Number> barChart;
	private Menu bereich = new Menu("Bereich");
	private GridPane grid = new GridPane();

	@Override
	public void start(Stage stage) throws Exception {
		boolean logged = false;
		Pane logPane = new Pane();
		logPane.setPrefSize(444.44, 190);
		GridPane loginGrid = new GridPane();
		Stage loginWindow = new Stage();
		LogInController logincontroller = new LogInController(logPane, loginGrid, logged, loginWindow);
		loginGrid = logincontroller.initLoginGrid();
		
		logPane.getChildren().add(loginGrid);
		Scene login = new Scene(logPane);
		
		loginWindow.setScene(login);
		loginWindow.setTitle("Login");
		loginWindow.showAndWait();
		if (logincontroller.getLogged()) {
			grid.setPrefSize(mainPane.getPrefWidth(), mainPane.getPrefHeight() - menuBar.getPrefHeight());
			grid.setVgap(6);
			grid.setHgap(6);
			mainPane.setPrefSize(1067, 600);
			mainPane.getChildren().add(vbox);
			menuBar.setPrefSize(mainPane.getPrefWidth(), 27.0);
			menuBar.getMenus().addAll(datei, bereich);
			menuBar.setEffect(new DropShadow(5, Color.gray(0.5)));
			datei.getItems().addAll(laden, speichern, dashboard);
			MainStageController msc = new MainStageController(mainPane, vbox, menuBar, bereich, datei, laden, speichern,
					dashboard, grid, barChart);
			
			msc.setMenuBarHandler();
			vbox.getChildren().addAll(menuBar, grid);

			Scene mainScene = new Scene(mainPane);
			stage.setTitle("DeSBL Caster�");
			stage.setScene(mainScene);
			stage.show();
		} else {

		}
	}

}
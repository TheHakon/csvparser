package gui.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class LogInParser {

	public LogInParser() {
	}

	private File loginData = new File(System.getProperty("user.dir").concat("\\user\\login_data.txt"));
	private HashMap<String, String> userData;

	public void parseUser() throws FileNotFoundException, IOException {
		try (BufferedReader r = new BufferedReader(new FileReader(loginData))) {
			String line;
			HashMap<String, String> userHM = new HashMap<String, String>();
			while ((line = r.readLine()) != null) {
				String[] hilfsArray = line.split(",");
				if (hilfsArray.length == 2) {
					userHM.put(hilfsArray[0], hilfsArray[1]);
				}
			}
			userData = userHM;
		}
	}
	
	public HashMap<String, String> getUserData(){
		return userData;
	}
	
}

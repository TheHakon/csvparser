package gui.controller;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class LogInController {

	public LogInController(Pane logPane, GridPane loginGrid, boolean logged, Stage loginWindow) {
		this.logPane = logPane;
		this.loginGrid = loginGrid;
		this.logged = logged;
		this.loginWindow = loginWindow;
	}

	private Pane logPane;
	private GridPane loginGrid;
	private boolean logged;
	private Stage loginWindow;
	public GridPane initLoginGrid() {
		loginGrid.setMinSize(logPane.getPrefWidth(), logPane.getPrefHeight());
		loginGrid.setMaxSize(logPane.getPrefWidth(), logPane.getPrefHeight());
		loginGrid.setHgap(6);
		loginGrid.setVgap(6);
		Label login = new Label("Login");
		login.setPrefWidth(logPane.getPrefWidth());
		login.setAlignment(Pos.CENTER);
		login.setStyle("-fx-font-size:60;" + "-fx-font-weight: bolder;");
		login.setEffect(new DropShadow(6,6,6, Color.LIGHTGRAY));
		login.setUnderline(true);
		TextField username = new TextField();
		username.setMaxWidth(logPane.getPrefWidth()/3);
		Label usernameLabel = new Label("Benutzername: ");
		usernameLabel.setPrefWidth(logPane.getPrefWidth()/2);
		usernameLabel.setAlignment(Pos.CENTER_RIGHT);
		usernameLabel.setStyle("-fx-font-weight: bold");
		PasswordField password = new PasswordField();
		password.setMaxWidth(logPane.getPrefWidth()/3);
		Label passwordLabel = new Label("Passwort: ");
		passwordLabel.setPrefWidth(logPane.getPrefWidth()/2);
		passwordLabel.setAlignment(Pos.CENTER_RIGHT);
		passwordLabel.setStyle("-fx-font-weight: bold");
		Button loginButton = new Button("Anmelden");
		loginButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if(!username.getText().isEmpty() && !password.getText().isEmpty()) {
					try {
						logged = parseInput(username.getText(), password.getText());
						if(logged) {
							loginWindow.close();
						} else {
							Alert failed = new Alert(AlertType.ERROR);
							failed.setTitle("Meldung");
							failed.setHeaderText("Anmeldung fehlgeschlagen");
							failed.show();
						}
						
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			
			}
		});
		GridPane.setHalignment(loginButton, HPos.CENTER);
		loginGrid.add(login, 0, 0, 2, 1);
		loginGrid.add(usernameLabel, 0, 1);
		loginGrid.add(username, 1, 1);
		loginGrid.add(passwordLabel, 0, 2);
		loginGrid.add(password, 1, 2);
		loginGrid.add(loginButton, 0, 3, 2, 1);
		loginGrid.setGridLinesVisible(false);
		return loginGrid;
	}
	
	public boolean parseInput(String username, String password) throws FileNotFoundException, IOException {
		LogInParser lip = new LogInParser();
		lip.parseUser();
		HashMap<String, String> userData = lip.getUserData();
		if(userData.containsKey(username)) {
			if(userData.get(username).equals(password)){
				return true;
			}
		}
		return false;
	}
	
	public boolean getLogged() {
		return logged;
	}

}

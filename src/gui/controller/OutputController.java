package gui.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import gui.model.Person;
import javafx.collections.ObservableList;

public class OutputController {

	public OutputController(ObservableList<Person> r6Persons, ObservableList<Person> bf1Persons,
			ObservableList<Person> codPersons, ObservableList<Person> rlPersons, ObservableList<Person> lolPersons,
			ObservableList<Person> owPersons, ObservableList<Person> castacPersons, DataParser p) {
		this.r6Persons = r6Persons;
		this.bf1Persons = bf1Persons;
		this.codPersons = codPersons;
		this.rlPersons = rlPersons;
		this.lolPersons = lolPersons;
		this.owPersons = owPersons;
		this.castacPersons = castacPersons;
		this.dataParser = p;
	}

	private ObservableList<Person> r6Persons;
	private ObservableList<Person> bf1Persons;
	private ObservableList<Person> codPersons;
	private ObservableList<Person> rlPersons;
	private ObservableList<Person> lolPersons;
	private ObservableList<Person> owPersons;
	private ObservableList<Person> castacPersons;
	private DataParser dataParser;

	public void createCSVData() throws IOException {
		ArrayList<String[]> currentState = dataParser.returnCurrentState();
		BufferedWriter br = new BufferedWriter(
				new FileWriter(new File(System.getProperty("user.dir").concat("\\data\\updatedData.csv"))));
		int length = 0;
		for (int i = 0; i < currentState.size(); i++) {
			if (length < currentState.get(i).length) {
				length = currentState.get(i).length;
			}
		}
		for (int i = 0; i < currentState.size(); i++) {
			StringBuffer line = new StringBuffer();
			if (currentState.get(i).length < length) {
				for (int j = 0; j < currentState.get(i).length; j++) {
					line.append(currentState.get(i)[j] + ",");
				}
				for (int j = 0; j < (length - (currentState.get(i).length + 1)); j++) {
					line.append(",");
				}
				br.write(line.toString() + "\n");
			} else {
				for (int j = 0; j < length - 1; j++) {
					line.append(currentState.get(i)[j] + ",");
				}
				line.append(currentState.get(i)[22]);

				br.write(line.toString() + "\n");
			}
		}
		br.close();
	}
}

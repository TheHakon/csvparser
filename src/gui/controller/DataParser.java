package gui.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import gui.model.Person;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class DataParser {

	public DataParser(File file) {
		this.file = file;
	}

	private File file;

	public ObservableList<Person> parseR6S() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int start = 0;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == 0) {
				if (array.length > 0 && array[0].toString().contains("Aktivit")) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (array.length == 0 || array[0].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 6) {
				for (int j = 0; j < eintr�ge.get(i).length; j++) {
					data[j] = eintr�ge.get(i)[j];
				}
			} else {
				for (int j = 0; j < 6; j++) {
					if (!eintr�ge.get(i)[j].equals(" ") && !eintr�ge.get(i)[j].equals("")) {
						data[j] = eintr�ge.get(i)[j];
					} else {
						data[j] = "";
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public ObservableList<Person> parseBF1() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int start = 0;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == 0) {
				if (array.length > 0 && array[7].toString().contains("Aktivit")) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (7 > array.length || array[7].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 14) {
				int j = 0;
				int startingPoint = 7;
				while (startingPoint < eintr�ge.get(i).length) {
					data[j] = eintr�ge.get(i)[startingPoint];
					startingPoint++;
					j++;
				}
			} else {
				int k = 0;
				for (int j = 7; j < 13; j++) {

					if (!eintr�ge.get(i)[j].equals("")) {
						data[k] = eintr�ge.get(i)[j];
						k++;
					} else {
						data[k] = null;
						k++;
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public ObservableList<Person> parseCoD() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int start = 0;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == 0) {
				if (array.length > 0 && array[14].toString().contains("Aktivit")) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (14 > array.length || array[14].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 20) {
				int j = 0;
				int startingPoint = 14;
				while (startingPoint < eintr�ge.get(i).length) {
					data[j] = eintr�ge.get(i)[startingPoint];
					startingPoint++;
					j++;
				}
			} else {
				int k = 0;
				for (int j = 14; j < 20; j++) {

					if (!eintr�ge.get(i)[j].equals("")) {
						data[k] = eintr�ge.get(i)[j];
						k++;
					} else {
						data[k] = null;
						k++;
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public ObservableList<Person> parseRl() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int reihe = 0;
		int start = Integer.MAX_VALUE;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == Integer.MAX_VALUE) {
				if (array.length > 0 && array[0].toString().contains("Aktivit") && reihe == 0) {
					reihe++;
				} else if (array.length > 0 && array[0].toString().contains("Aktivit") && reihe == 1) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (array.length == 0 || array[0].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 6) {
				int j = 0;
				int startingPoint = 0;
				while (startingPoint < eintr�ge.get(i).length) {
					data[j] = eintr�ge.get(i)[startingPoint];
					startingPoint++;
					j++;
				}
			} else {
				int k = 0;
				for (int j = 0; j < 6; j++) {

					if (!eintr�ge.get(i)[j].equals("")) {
						data[k] = eintr�ge.get(i)[j];
						k++;
					} else {
						data[k] = null;
						k++;
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public ObservableList<Person> parseLol() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int reihe = 0;
		int start = Integer.MAX_VALUE;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == Integer.MAX_VALUE) {
				if (array.length > 7 && array[7].toString().contains("Aktivit") && reihe == 0) {
					reihe++;
				} else if (array.length > 7 && array[7].toString().contains("Aktivit") && reihe == 1) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (array.length < 8 || array[7].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 13) {
				int j = 0;
				int startingPoint = 7;
				while (startingPoint < eintr�ge.get(i).length) {
					data[j] = eintr�ge.get(i)[startingPoint];
					startingPoint++;
					j++;
				}
			} else {
				int k = 0;
				for (int j = 7; j < 13; j++) {

					if (!eintr�ge.get(i)[j].equals("")) {
						data[k] = eintr�ge.get(i)[j];
						k++;
					} else {
						data[k] = null;
						k++;
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public ObservableList<Person> parseOw() throws IOException {

		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int reihe = 0;
		int start = Integer.MAX_VALUE;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == Integer.MAX_VALUE) {
				if (array.length > 14 && array[14].toString().contains("Aktivit") && reihe == 0) {
					reihe++;
				} else if (array.length > 14 && array[14].toString().contains("Aktivit") && reihe == 1) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (array.length < 15 || array[14].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 20) {
				int j = 0;
				int startingPoint = 14;
				while (startingPoint < eintr�ge.get(i).length) {
					data[j] = eintr�ge.get(i)[startingPoint];
					startingPoint++;
					j++;
				}
			} else {
				int k = 0;
				for (int j = 14; j < 20; j++) {

					if (!eintr�ge.get(i)[j].equals("")) {
						data[k] = eintr�ge.get(i)[j];
						k++;
					} else {
						data[k] = null;
						k++;
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public ObservableList<Person> parseCastAc() throws IOException {
		int endzahl = 0;
		try (BufferedReader endLine = new BufferedReader(new FileReader(file))) {
			@SuppressWarnings("unused")
			String testEnd;
			while ((testEnd = endLine.readLine()) != null) {
				endzahl++;
			}
		}
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int reihe = 0;
		int start = Integer.MAX_VALUE;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == Integer.MAX_VALUE) {
				if (array.length > 14 && array[14].toString().contains("Aktivit") && (reihe == 0 || reihe == 1)) {
					reihe++;
				} else if (array.length > 14 && array[14].toString().contains("Aktivit") && reihe == 2) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (count == endzahl) {
					end = count;
				} else if (array.length < 15 || array[14].toString().equals("")) {
					end = (count - 1);
				}
			}
		}
		ObservableList<Person> ausgabe = FXCollections.observableArrayList();
		for (int i = start; i < end; i++) {
			String[] data = new String[6];
			if (eintr�ge.get(i).length < 20) {
				int j = 0;
				int startingPoint = 14;
				while (startingPoint < eintr�ge.get(i).length) {
					data[j] = eintr�ge.get(i)[startingPoint];
					startingPoint++;
					j++;
				}
			} else {
				int k = 0;
				for (int j = 14; j < 20; j++) {

					if (!eintr�ge.get(i)[j].equals("")) {
						data[k] = eintr�ge.get(i)[j];
						k++;
					} else {
						data[k] = null;
						k++;
					}
				}
			}
			Person p = new Person(data[0], data[1], data[2], data[3], data[4], data[5]);
			ausgabe.add(p);
		}
		br.close();
		return ausgabe;
	}

	public int countAktiveCaster(ObservableList<Person> personen) {
		int anzahlCaster = 0;
		for(int i = 0; i<personen.size(); i++) {
			Person x = personen.get(i);
			if(x.getAktivit�t().equalsIgnoreCase("aktiv")) {
				anzahlCaster++;
			}
		}
		return anzahlCaster;
	}

	public HashMap<String, Number> countCasts() throws IOException{
		HashMap<String, Number> castCount = new HashMap<String, Number>();
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		int start = Integer.MAX_VALUE;
		int end = 0;
		int count = 0;
		while ((line = br.readLine()) != null) {
			count++;
			array = line.split(",");
			eintr�ge.add(array);
			if (start == Integer.MAX_VALUE) {
				if (array.length > 21 && array[21].toString().contains("Caster")) {
					start = (count + 1);
				}
			}
			if (end == 0 && count > start) {
				if (array.length < 21 || array[21].toString().isEmpty()) {
					end = (count - 1);
				}
			}
		}
		for(int i = start; i<end; i++) {
			if(eintr�ge.get(i).length>22) {
				castCount.put(eintr�ge.get(i)[21], new Integer(eintr�ge.get(i)[22]));
			} else {
				castCount.put(eintr�ge.get(i)[21], 0);
			}
		}
		br.close();
		return castCount;
	}
	
	public ArrayList<String[]> returnCurrentState() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(file));
		ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
		String line;
		String[] array;
		while ((line = br.readLine()) != null) {
			array = line.split(",");
			eintr�ge.add(array);
		}
		br.close();
		return eintr�ge;
	}
}

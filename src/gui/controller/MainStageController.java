package gui.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.event.ChangeEvent;

import download.FileLoader;
import gui.model.Person;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Modality;

public class MainStageController {

	public MainStageController(Pane mainPane, VBox vbox, MenuBar menuBar, Menu bereich, Menu datei, MenuItem laden,
			MenuItem speichern, MenuItem dashboard, GridPane grid, BarChart<String, Number> barChart) {

		this.mainPane = mainPane;
		this.grid = grid;
		this.barChartAlleCaster = barChart;
		this.menuBar = menuBar;
		this.dashboard = dashboard;
		this.laden = laden;
		this.speichern = speichern;
		this.bereich = bereich;
	}

	private Pane mainPane;
	private Menu bereich;
	private MenuBar menuBar;
	private ObservableList<MenuItem> bereiche = FXCollections.observableArrayList();
	private BarChart<String, Number> barChartAlleCaster;
	private MenuItem dashboard;
	private MenuItem laden;
	private MenuItem speichern;
	private MenuItem rainbow;
	private MenuItem bf1;
	private MenuItem cod;
	private MenuItem rl;
	private MenuItem lol;
	private MenuItem ow;
	private MenuItem castac;
	private GridPane grid;
	private File file;
	@SuppressWarnings("rawtypes")
	private TableColumn aktivit�t = new TableColumn("Aktivit�t");
	@SuppressWarnings("rawtypes")
	private TableColumn spielername = new TableColumn("Spielername");
	@SuppressWarnings("rawtypes")
	private TableColumn discordname = new TableColumn("Discordname");
	@SuppressWarnings("rawtypes")
	private TableColumn plattform = new TableColumn("Plattform");
	@SuppressWarnings("rawtypes")
	private TableColumn plattformPC = new TableColumn("PC");
	@SuppressWarnings("rawtypes")
	private TableColumn plattformXBOX = new TableColumn("XBOX");
	@SuppressWarnings("rawtypes")
	private TableColumn plattformPS4 = new TableColumn("PS4");
	private Button pc = new Button("PC");
	private Button xbox = new Button("XBOX");
	private Button ps4 = new Button("PS4");
	private Button all = new Button("Alle");
	private Label anzahlCasts = new Label();

	private Alert fileNotFound = new Alert(AlertType.ERROR);
	private Alert fileFound = new Alert(AlertType.INFORMATION);

	@SuppressWarnings("unchecked")
	public void clickOnLaden() throws MalformedURLException, IOException {
		FileLoader fl = new FileLoader();
		boolean downloadSuccess = true;
		try {
			fl.downloadFile();
		} catch (UnknownHostException e) {
			Alert noConnection = new Alert(AlertType.ERROR);
			noConnection.setTitle("Verbindungsproblem");
			noConnection.setHeaderText("Es konnte keine Verbindung hergestellt werden");
			noConnection.setContentText("Die zuletzt geladene Datei wird verwendet");
			downloadSuccess = false;
			noConnection.showAndWait();
		}

		file = new File(System.getProperty("user.dir").concat("\\data\\data.csv"));
		if (file == null) {
			fileNotFound.setTitle("Fehler");
			fileNotFound.setHeaderText("Dateifehler");
			fileNotFound.setContentText("Es wurde keine g�ltige Datei ausgew�hlt!");
			fileNotFound.initModality(Modality.APPLICATION_MODAL);
			fileNotFound.show();

		} else {
			if (downloadSuccess) {
				fileFound.setTitle("Erfolg");
				fileFound.setHeaderText("Datei heruntergeladen");
				fileFound.setContentText("Die Datei wurde heruntergeladen!");
				fileFound.show();
			}
			grid.getChildren().clear();
			initDashboardPage();
			try {
				bereiche.clear();
				plattform.getColumns().clear();
				DataParser p = new DataParser(file);
				pc.setMaxWidth(Double.MAX_VALUE);
				xbox.setMaxWidth(Double.MAX_VALUE);
				ps4.setMaxWidth(Double.MAX_VALUE);
				all.setMaxWidth(Double.MAX_VALUE);
				grid.setMinHeight(250);
				grid.setPrefSize(mainPane.getPrefWidth(), mainPane.getPrefHeight() - menuBar.getPrefHeight());
				grid.setGridLinesVisible(true);
				plattform.getColumns().addAll(plattformPC, plattformPS4, plattformXBOX);
				aktivit�t.setCellValueFactory(new PropertyValueFactory<>("aktivit�t"));
				spielername.setCellValueFactory(new PropertyValueFactory<>("spielername"));
				discordname.setCellValueFactory(new PropertyValueFactory<>("discordname"));
				plattformPC.setCellValueFactory(new PropertyValueFactory<>("plattformpc"));
				plattformPS4.setCellValueFactory(new PropertyValueFactory<>("plattformps4"));
				plattformXBOX.setCellValueFactory(new PropertyValueFactory<>("plattformxbox"));
				plattform.setStyle("-fx-alignment: CENTER;");
				plattformPC.setStyle("-fx-alignment: CENTER;");
				plattformPS4.setStyle("-fx-alignment: CENTER;");
				plattformXBOX.setStyle("-fx-alignment: CENTER;");
				BufferedReader br = new BufferedReader(new FileReader(file));
				ArrayList<String[]> eintr�ge = new ArrayList<String[]>();
				String line;
				while ((line = br.readLine()) != null) {
					String[] zeile = line.split(",");
					eintr�ge.add(zeile);
					for (int i = 0; i < zeile.length; i++) {
						if (zeile[i].contains("Aktivit")) {
							switch (zeile[i + 1]) {
							case "Rainbow Six Siege":
								rainbow = new MenuItem("Rainbow Six Siege");
								if (!bereiche.contains(rainbow)) {
									bereiche.add(rainbow);
									rainbow.setOnAction(new EventHandler<ActionEvent>() {
										@SuppressWarnings("rawtypes")
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("Rainbow Six Siege");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseR6S();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							case "Battlefield 1":
								bf1 = new MenuItem("Battlefield 1");
								if (!bereiche.contains(bf1)) {
									bereiche.add(bf1);
									bf1.setOnAction(new EventHandler<ActionEvent>() {
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("Battlefield 1");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseBF1();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							case "Call of Duty - Aktuelles Spiel":
								cod = new MenuItem("Call of Duty");
								if (!bereiche.contains(cod)) {
									bereiche.add(cod);
									cod.setOnAction(new EventHandler<ActionEvent>() {
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("Call of Duty");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseCoD();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							case "Rocket League":
								rl = new MenuItem("Rocket League");
								if (!bereiche.contains(rl)) {
									bereiche.add(rl);
									rl.setOnAction(new EventHandler<ActionEvent>() {
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("Rocket League");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseRl();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							case "League of Legends":
								lol = new MenuItem("League of Legends");
								if (!bereiche.contains(lol)) {
									bereiche.add(lol);
									lol.setOnAction(new EventHandler<ActionEvent>() {
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("League of Legends");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseLol();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							case "Overwatch":
								ow = new MenuItem("Overwatch");
								if (!bereiche.contains(ow)) {
									bereiche.add(ow);
									ow.setOnAction(new EventHandler<ActionEvent>() {
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("Overwatch");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseOw();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							case "Caster Academy":
								castac = new MenuItem("Caster Academy");
								if (!bereiche.contains(castac)) {
									bereiche.add(castac);
									castac.setOnAction(new EventHandler<ActionEvent>() {
										@Override
										public void handle(ActionEvent event) {
											Label game = new Label("Caster Academy");
											TableView<Person> tableView = initTable();
											ObservableList<Person> persons = FXCollections.observableArrayList();
											initLabel(game);
											try {
												persons = p.parseCastAc();
												tableView.getItems().addAll(persons);
												setFilterButtonHandler(tableView, persons);
												setCastCountListener(tableView);
											} catch (IOException e) {
												System.out.println(e.getCause());
												e.printStackTrace();
											}
										}
									});
								}
								break;
							}
						}
					}
				}
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public TableView<Person> initTable() {
		grid.getChildren().clear();
		TableView<Person> tableView = new TableView<Person>();
		tableView.setMinSize(mainPane.getPrefWidth() / 2, 400);
		tableView.getColumns().addAll(aktivit�t, spielername, discordname, plattform);
		// grid.setGridLinesVisible(true);
		VBox vboxForButtons = new VBox();
		vboxForButtons.setMaxWidth(100);
		vboxForButtons.setSpacing(5);
		vboxForButtons.getChildren().addAll(pc, xbox, ps4, all);
		VBox vboxForInfos = new VBox();
		vboxForInfos.setPrefHeight(tableView.getPrefHeight());
		vboxForInfos.getChildren().add(anzahlCasts);
		vboxForInfos.setSpacing(5);
		vboxForInfos.setStyle("-fx-padding: 5;"
				+ "-fx-border-style: solid;"
				+ "-fx-border-width: 1;"
				+ "-fx-border-color: lightgrey;");
		grid.getChildren().addAll(tableView, vboxForButtons, vboxForInfos);
		GridPane.setConstraints(tableView, 0, 1);
		GridPane.setConstraints(vboxForButtons, 1, 1);
		GridPane.setConstraints(vboxForInfos, 2, 1);
		return tableView;
	}

	public void initLabel(Label game) {
		game.setPrefWidth(mainPane.getPrefWidth());
		game.setAlignment(Pos.CENTER);
		game.setStyle("-fx-font-size:50;" + "-fx-font-weight: bolder;"
				+ "-fx-background-color: linear-gradient(#e2e2e2 0%,#ffffff 100%)");
		game.setEffect(new DropShadow(5, Color.gray(0.5)));
		game.setUnderline(true);

		grid.add(game, 0, 0, 3, 1);
	}


	public void initDashboardPage() throws IOException {
		DataParser p = new DataParser(file);
		grid.getChildren().clear();
		final CategoryAxis xAxisAlleCaster = new CategoryAxis();
		final NumberAxis yAxisAlleCaster = new NumberAxis();
		barChartAlleCaster = new BarChart<String, Number>(xAxisAlleCaster, yAxisAlleCaster);
		barChartAlleCaster.setMinSize(mainPane.getPrefWidth() / 3,
				(mainPane.getPrefHeight() - menuBar.getPrefHeight()));
		barChartAlleCaster.setTitle("Anzahl aller Caster");
		xAxisAlleCaster.setLabel("Bereich");
		yAxisAlleCaster.setLabel("Anzahl");
		XYChart.Series<String, Number> datensatz2018AlleCaster = new XYChart.Series<String, Number>();
		datensatz2018AlleCaster.setName("Alle Caster - Stand 2018");
		datensatz2018AlleCaster.getData()
				.add(new XYChart.Data<String, Number>("Rainbow Six Siege", p.parseR6S().size()));
		datensatz2018AlleCaster.getData().add(new XYChart.Data<String, Number>("Battlefield 1", p.parseBF1().size()));
		datensatz2018AlleCaster.getData().add(new XYChart.Data<String, Number>("Call of Duty", p.parseCoD().size()));
		datensatz2018AlleCaster.getData().add(new XYChart.Data<String, Number>("Rocket League", p.parseRl().size()));
		datensatz2018AlleCaster.getData()
				.add(new XYChart.Data<String, Number>("League of Legends", p.parseLol().size()));
		datensatz2018AlleCaster.getData().add(new XYChart.Data<String, Number>("Overwatch", p.parseOw().size()));
		datensatz2018AlleCaster.getData()
				.add(new XYChart.Data<String, Number>("Caster Academy", p.parseCastAc().size()));
		XYChart.Series<String, Number> datensatz2018AktiveCaster = new XYChart.Series<String, Number>();
		datensatz2018AktiveCaster.setName("Aktive Caster - Stand 2018");
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("Rainbow Six Siege", p.countAktiveCaster(p.parseR6S())));
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("Battlefield 1", p.countAktiveCaster(p.parseBF1())));
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("Call of Duty", p.countAktiveCaster(p.parseCoD())));
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("Rocket League", p.countAktiveCaster(p.parseRl())));
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("League of Legends", p.countAktiveCaster(p.parseLol())));
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("Overwatch", p.countAktiveCaster(p.parseOw())));
		datensatz2018AktiveCaster.getData()
				.add(new XYChart.Data<String, Number>("Caster Academy", p.countAktiveCaster(p.parseCastAc())));
		barChartAlleCaster.getData().add(datensatz2018AlleCaster);
		barChartAlleCaster.getData().add(datensatz2018AktiveCaster);
		VBox casterGenaueZahlListeAlleCaster = new VBox();
		Label r6casterGenaueZahlAlleCaster = new Label("Anzahl f�r Rainbow Six Siege: " + p.parseR6S().size());
		Label bf1casterGenaueZahlAlleCaster = new Label("Anzahl f�r Battlefield 1: " + p.parseBF1().size());
		Label codcasterGenaueZahlAlleCaster = new Label("Anzahl f�r Call of Duty: " + p.parseCoD().size());
		Label rlcasterGenaueZahlAlleCaster = new Label("Anzahl f�r Rocket League: " + p.parseRl().size());
		Label lolcasterGenaueZahlAlleCaster = new Label("Anzahl f�r League of Legends: " + p.parseLol().size());
		Label owcasterGenaueZahlAlleCaster = new Label("Anzahl f�r Overwatch: " + p.parseOw().size());
		Label casteraccasterGenaueZahlAlleCaster = new Label("Anzahl f�r Caster Academy: " + p.parseCastAc().size());
		casterGenaueZahlListeAlleCaster.getChildren().addAll(r6casterGenaueZahlAlleCaster,
				bf1casterGenaueZahlAlleCaster, codcasterGenaueZahlAlleCaster, rlcasterGenaueZahlAlleCaster,
				lolcasterGenaueZahlAlleCaster, owcasterGenaueZahlAlleCaster, casteraccasterGenaueZahlAlleCaster);
		grid.add(barChartAlleCaster, 0, 0);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setCastCountListener(TableView<Person> tableView) {
		DataParser p = new DataParser(file);
		tableView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				if (newValue != null) {
					Person person = (Person) newValue;
					HashMap<String, Number> castCounts;
					try {
						castCounts = p.countCasts();
						if (castCounts.containsKey(person.getSpielername())) {
							anzahlCasts.setText("Anzahl der absolvierten Casts:   "
									+ castCounts.get(person.getSpielername()).toString());
							anzahlCasts.setStyle("-fx-font-weight: bold; -fx-font-size: 20;");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			}
		});

	}

	public void setMenuBarHandler() {
		laden.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				try {
					clickOnLaden();
				} catch (IOException e) {
					e.printStackTrace();
				}
				bereich.getItems().clear();
				bereich.getItems().addAll(getBereiche());
			}
		});
		speichern.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				DataParser p = new DataParser(file);
				try {
					OutputController oc = new OutputController(p.parseR6S(), p.parseBF1(), p.parseCoD(), p.parseRl(), p.parseLol(), p.parseOw(), p.parseCastAc(), p);
					oc.createCSVData();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});
		dashboard.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (file != null && file.exists()) {
					try {
						initDashboardPage();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}

	public void setFilterButtonHandler(TableView<Person> tableView, ObservableList<Person> persons) {
		final ObservableList<Person> x = persons;
		Object[] personArray = x.toArray();
		all.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableView.getItems().clear();
				tableView.getItems().addAll(x);
			}
		});
		pc.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableView.getItems().clear();

				for (int i = 0; i < personArray.length; i++) {
					Person x = (Person) personArray[i];
					try {
						if (!x.getPlattformpc().isEmpty()) {
							tableView.getItems().add(x);
						}
					} catch (NullPointerException e) {

					}
				}
			}
		});
		ps4.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableView.getItems().clear();
				for (int i = 0; i < personArray.length; i++) {
					Person x = (Person) personArray[i];
					try {
						if (!x.getPlattformps4().isEmpty()) {
							tableView.getItems().add(x);
						}
					} catch (NullPointerException e) {

					}
				}
			}
		});
		xbox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				tableView.getItems().clear();
				for (int i = 0; i < personArray.length; i++) {
					Person x = (Person) personArray[i];
					try {
						if (!x.getPlattformxbox().isEmpty()) {
							tableView.getItems().add(x);
						}
					} catch (NullPointerException e) {

					}
				}
			}
		});
	}

	public ObservableList<MenuItem> getBereiche() {
		return bereiche;
	}

	public GridPane getGrid() {
		return grid;
	}
}
